from django.db import models

# Create your models here.

class MovieActor(models.Model):
    actor = models.CharField(max_length=200)
    movie = models.CharField(max_length=200)
    id_actor = models.CharField(max_length=200)
    id_movie = models.CharField(max_length=200)
