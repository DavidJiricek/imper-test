from django import forms

class NameForm(forms.Form):
    pattern = forms.CharField(label='Zadej jméno herce nebo filmu:', max_length=100)
