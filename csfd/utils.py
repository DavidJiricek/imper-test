import unidecode as unidec

def normalize_str(accended_string):
    temp = unidec.unidecode(accended_string)
    temp = temp.lower()
    return temp.replace(' ','-')
