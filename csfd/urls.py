from django.urls import path

from . import views

urlpatterns = [
    path('', views.input_form, name='input_form'),
    path('actor/<str:id_actor>/', views.actor, name='actor'),
    path('movie/<str:id_movie>/', views.movie, name='movie'),
]
