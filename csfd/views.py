from django.shortcuts import render
from django.http import HttpResponse

from .forms import NameForm
from .models import MovieActor
from .utils import *

"""
    Stránka obsahující input form pro vyhledání filmů a herců.
"""
def input_form(request):

    if request.method == 'POST':
        form = NameForm(request.POST)
        pattern = normalize_str(request.POST['pattern'])
        movie_results = MovieActor.objects.filter(id_movie__contains=pattern).values('movie', 'id_movie').distinct()
        actor_results = MovieActor.objects.filter(id_actor__contains=pattern).values('actor', 'id_actor').distinct()

    else:
        form = NameForm()
        movie_results = None
        actor_results = None

    return render(request, 'csfd/results.html', {'form': form, 'movie_results' : movie_results, 'actor_results' : actor_results})

"""
    Detail herce.
"""
def actor(request, id_actor):
    name = MovieActor.objects.filter(id_actor=id_actor).values_list('actor')[0][0]
    all_movies = MovieActor.objects.filter(id_actor=id_actor).values('movie', 'id_movie').distinct()
  
    return render(request, 'csfd/actor.html', {'name' : name, 'all_movies': all_movies})

"""
    Detail filmu.
"""
def movie(request, id_movie):
    name = MovieActor.objects.filter(id_movie=id_movie).values_list('movie')[0][0]
    all_actors = MovieActor.objects.filter(id_movie=id_movie).values('actor', 'id_actor').distinct()
  
    return render(request, 'csfd/movie.html', {'name' : name, 'all_actors': all_actors})

