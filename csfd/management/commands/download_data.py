from django.core.management.base import BaseCommand, CommandError
import requests as req
import sys
from lxml import html
from collections import defaultdict
from csfd.models import MovieActor
from csfd.utils import *

class Command(BaseCommand):
    help = 'Downloads movie and actors data from CSFD database.'

    def handle(self, *args, **options):
        #MovieActor.objects.all().delete()

        records = []

        csfd_url = "https://www.csfd.cz/zebricky/nejlepsi-filmy/?show=complete"
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0'}

        response = req.get(csfd_url, headers=headers)
        tree = html.fromstring(response.content)
        zebricek = tree.xpath('//table[@class="content ui-table-list striped"]')[0]
        films = zebricek.xpath('.//td[@class="film"]')[0:300]
        
        progress_counter=0;
        print('Downloading from CSFD database... ', end='')
        for film in films:
           progress_counter += 1
           if (progress_counter%10==0):
               print('#', end='', flush=True)

           film_url = "https://www.csfd.cz" + film[0].get('href')
           film_response = req.get(film_url, headers=headers)
           
           film_tree = html.fromstring(film_response.content)
           creators = film_tree.xpath('.//div[@class="creators"]')[0]
           actors = creators.xpath('.//h4[text()="Hrají:"]/ancestor::div[1]//a')

           for actor in actors:
               movie_name = film[0].text
               id_movie = normalize_str(film[0].text)
               id_actor = normalize_str(actor.text)
               actor_name = actor.text
               records.append(MovieActor(actor=actor_name, movie=movie_name, id_actor=id_actor, id_movie=id_movie))

        MovieActor.objects.bulk_create(records)
        self.stdout.write("\nData successfully downloaded.")

	
	
