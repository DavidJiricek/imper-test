Struktura programu
------------------

Vše podstatné se odehrává v aplikaci **csfd**.

Data jsou již staženy do databáze. 

Skript na stažení dat je implementovaný jako management command pod názvem **download_data**.
Skript se nestará o vymazaní databáze - v případě nového spuštění se nová data přidají ke stávajícím.

Databáze má pouze jednou tabulku "csfd_movieactor", obsahující dvojice "herec <=> film, ve kterém hrál".

Program neřeší možné duplicity jména herců nebo filmů (dva herci toho samého jména budou reprezentováni jako jeden).



